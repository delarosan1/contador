/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import CustomButtom from './components/button';
import ActionButtons from './components/actionButtons';

class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      counter: 0,
    };

    this.handleUp = this.handleUp.bind(this);
    this.handleDown = this.handleDown.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handlePlus10 = this.handlePlus10.bind(this);
    this.handleMinus10 = this.handleMinus10.bind(this);
  }

  shouldComponentUpdate(nextProps: Readonly<any>, nextState: Readonly<any>, nextContext: any): boolean {
    const { counter } = this.state;
    if (nextState.counter === counter) return false;

    return true;
  }

  handleUp() {
    const { counter: ct } = this.state;
    this.setState({counter: ct + 1 });
  }

  handleDown() {
    const { counter: ct } = this.state;
    this.setState({counter: ct - 1 });
  }

  handleReset() {
    this.setState({counter: 0 });
  }

  handlePlus10() {
    const { counter: ct } = this.state;
    this.setState({counter: ct + 10 });
  }

  handleMinus10() {
    const { counter: ct } = this.state;
    this.setState({counter: ct - 10 });
  }

  render() {
    const { counter } = this.state;

    return(
      <View style={styles.container}>
          <View style={styles.subcontainer}>
            
            <CustomButtom label='-' action={this.handleDown}/>

            <View style={styles.counterContainer}>
              <Text style={styles.counter}>{counter}</Text>
            </View>
            
            <CustomButtom label='+' action={this.handleUp}/>

          </View>
          <View style={styles.subcontainerReset}>
            <ActionButtons 
              reset={this.handleReset}
              plus10={this.handlePlus10}
              minus10={this.handleMinus10}/>
          </View>
      </View>
    );
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#34495e',
    justifyContent: 'center',
  },
  subcontainer: {
    height: 50,
    width: '100%',
    paddingHorizontal: 10,
    flexDirection: 'row',
  },
  subcontainerReset: {
    height: 50,
    width: '100%',
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  counterContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  counter: {
    fontSize: 45,
    color: '#FFF',
    fontWeight: 'bold',
  },
});

export default App;
