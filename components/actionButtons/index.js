import React, { Component, Fragment } from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

class ActionButtons extends Component {
    render() {
        const {reset, plus10, minus10} = this.props;
        return(
        <Fragment>
            <TouchableOpacity 
            style={styles.btnReset}
            onPress={minus10}
            >
                <Text style={styles.btnTxt}>- 10</Text>
            </TouchableOpacity>

            <TouchableOpacity 
            style={styles.btnReset}
            onPress={reset}
            >
                <Text style={styles.btnTxt}>Reset</Text>
            </TouchableOpacity>

            <TouchableOpacity 
            style={styles.btnReset}
            onPress={plus10}
            >
                <Text style={styles.btnTxt}>+ 10</Text>
            </TouchableOpacity>
        </Fragment>
        
        );
    }
}

const styles = StyleSheet.create({
    btnTxt: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#000',
      },
      btnReset: {
        width: 100,
        height: 50,
        marginHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ecf0f1',
      },
});

export default ActionButtons;