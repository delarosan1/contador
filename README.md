# Contador
Esta es una aplicación de prueba de un simple contador creada con la tecnología React Native CLI.

## Download the proyect
```
git clone https://gitlab.com/delarosan1/contador.git
git pull
```

## Getting started
```
yarn
cd ios
pod install
cd ..
```

## Run Android
npx react-native run-android

## Run iOS
npx react-native run-ios --simulator="iPhone 14"

## Documentacion
https://reactnative.dev/docs/environment-setup

## AUTOR
Juan José de la Rosa